<?php

declare(strict_types=1);

namespace [[namespace]];

use Garrcomm\EasyMigrations\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class [[classname]] extends AbstractMigration
{
    public function up(): void
    {
        // this up() migration is auto-generated, please modify it to your needs by adding $this->addSql() lines
    }

    public function down(): void
    {
        // this down() migration is auto-generated, please modify it to your needs by adding $this->addSql() lines
    }
}
