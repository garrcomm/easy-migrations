<?php

namespace Garrcomm\EasyMigrations\Service;

class ConsoleOutput
{
    /**
     * Writes data to the output console
     *
     * @param string $output The output.
     *
     * @return void
     */
    public function stdout(string $output): void
    {
        fputs(STDOUT, $output);
    }

    /**
     * Writes data to the error console
     *
     * @param string $output The output.
     *
     * @return void
     */
    public function stderr(string $output): void
    {
        fputs(STDERR, $output);
    }
}
