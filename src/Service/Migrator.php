<?php

namespace Garrcomm\EasyMigrations\Service;

use Garrcomm\EasyMigrations\Action\Up;
use Garrcomm\EasyMigrations\Action\Down;
use Garrcomm\EasyMigrations\Action\Execute;
use Garrcomm\EasyMigrations\Action\Generate;
use Garrcomm\EasyMigrations\Action\Help;
use Garrcomm\EasyMigrations\Model\Config;
use InvalidArgumentException;
use RuntimeException;
use Throwable;

class Migrator
{
    /**
     * Reference to the console output
     *
     * @var ConsoleOutput
     */
    private $console;

    /**
     * The command called from the command line
     *
     * @var string
     */
    private $commandCall;

    /**
     * Contains the action to execute
     *
     * @var string|null
     */
    private $action = null;

    /**
     * A list of additional config files
     *
     * @var string[]
     */
    private $additionalConfigs = array();

    /**
     * Config data
     *
     * @var Config
     */
    private $config;

    /**
     * @var int|null
     */
    private $version = null;

    /**
     * Kicks off the migrator command and returns an exit code
     *
     * @param string[] $arguments All command line arguments.
     *
     * @return int
     */
    public function run(array $arguments, ConsoleOutput $output): int
    {
        try {
            $this->config = new Config();
            $this->console = $output;
            $this->parseArguments($arguments);

            // Arguments parsed successfully, continue
            $this->loadConfig();

            // Execute generate action
            if ($this->action === 'help') {
                return (new Help($this->config, $this->console))->setCommandCall($this->commandCall)->run();
            } elseif ($this->action == 'generate') {
                return (new Generate($this->config, $this->console))->run();
            } elseif ($this->action == 'execute') {
                return (new Execute($this->config, $this->console))->run();
            } elseif ($this->action == 'up') {
                return (new Up($this->config, $this->console))->setVersion((int)$this->version)->run();
            } elseif ($this->action == 'down') {
                return (new Down($this->config, $this->console))->setVersion((int)$this->version)->run();
            }

            throw new InvalidArgumentException('No action to execute. Try --help');
        } catch (Throwable $throwable) {
            $this->console->stderr(
                get_class($throwable) . ($throwable->getCode() ? ' #' . $throwable->getCode() : '') . ': '
                . $throwable->getMessage() . PHP_EOL
                . '  ' . $throwable->getFile() . ':' . $throwable->getLine() . PHP_EOL
            );
            return 1;
        }
    }

    /**
     * Parses all command line arguments
     *
     * @param string[] $arguments Array of the arguments.
     *
     * @return void
     * @throws InvalidArgumentException Thrown when an argument is invalid.
     */
    private function parseArguments(array $arguments): void
    {
        $this->commandCall = (string)array_shift($arguments);
        while (count($arguments)) {
            $argument = array_shift($arguments);
            if ($argument == '--help') {
                $this->action = 'help';
            } elseif ($argument == '--generate') {
                $this->action = 'generate';
            } elseif ($argument == '--execute') {
                $this->action = 'execute';
            } elseif ($argument == '--down') {
                $this->action = 'down';
                $version = array_shift($arguments);
                if (!$version) {
                    throw new InvalidArgumentException('No version specified after --down');
                } elseif (!preg_match('/^[0-9]{14}$/', $version)) {
                    throw new InvalidArgumentException('Version must contain 14 numbers, not ' . $version);
                }
                $this->version = (int)$version;
            } elseif ($argument == '--up') {
                $this->action = 'up';
                $version = array_shift($arguments);
                if (!$version) {
                    throw new InvalidArgumentException('No version specified after --up');
                } elseif (!preg_match('/^[0-9]{14}$/', $version)) {
                    throw new InvalidArgumentException('Version must contain 14 numbers, not ' . $version);
                }
                $this->version = (int)$version;
            } elseif ($argument == '--config') {
                $key = array_shift($arguments);
                $value = array_shift($arguments);
                if (!$key || !$value) {
                    throw new InvalidArgumentException('No key value pair specified after --config');
                }
                $this->config->addConfigFlag($key, $value, $this->commandCall, -1);
            } elseif ($argument == '--config-file') {
                $configFile = array_shift($arguments);
                if (!$configFile) {
                    throw new InvalidArgumentException('No path specified after --config');
                } elseif (!file_exists($configFile)) {
                    throw new InvalidArgumentException('The file ' . $configFile . ' can\'t be found');
                } elseif (!is_readable($configFile)) {
                    throw new InvalidArgumentException('The file ' . $configFile . ' can\'t be read');
                }
                $this->additionalConfigs[] = $configFile;
            } else {
                throw new InvalidArgumentException('Argument ' . $argument . ' invalid. Try --help');
            }
        }
    }

    /**
     * Looks for a config file, if none is found, a runtime exception is thrown.
     *
     * @return void
     * @throws RuntimeException Thrown when no config is found, or config is unreadable.
     */
    private function loadConfig(): void
    {
        $executablePath = pathinfo((string)realpath($this->commandCall), PATHINFO_DIRNAME);
        // Possible locations of the config file, in priority sequence (first row has the lowest priority)
        $locations = array_merge([
            $executablePath . '/migrator.ini',
            $executablePath . '/migrator.ini.dist',
            getcwd() . '/config/migrator.ini',
            getcwd() . '/config/migrator.ini.dist',
            getcwd() . '/migrator.ini',
            getcwd() . '/migrator.ini.dist',
        ], $this->additionalConfigs);

        foreach ($locations as $priority => $location) {
            if (file_exists($location)) {
                $configData = (array)parse_ini_file($location, true, INI_SCANNER_TYPED);
                foreach ($configData['migrator'] as $key => $value) {
                    $this->config->addConfigFlag($key, $value, $location, $priority);
                }
            }
        }
        if (!count($this->config) && $this->action != 'help') {
            throw new RuntimeException(
                'No config found. Create a migrator.ini(.dist) or '
                . 'specify with --config [key] [value] or --config-path [path]'
            );
        }
    }
}
