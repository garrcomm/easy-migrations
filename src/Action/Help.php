<?php

namespace Garrcomm\EasyMigrations\Action;

class Help extends AbstractAction
{
    /**
     * The command called from the command line
     *
     * @var string
     */
    private $commandCall;

    /**
     * Sets the command called from the command line
     *
     * @param string $commandCall The command called from the command line.
     *
     * @return $this
     */
    public function setCommandCall(string $commandCall): self
    {
        $this->commandCall = $commandCall;
        return $this;
    }

    /**
     * Executes the action and returns an exit code
     *
     * @return int
     */
    public function run(): int
    {
        $text = 'Usage:' . PHP_EOL
            . '  ' . $this->commandCall . ' [action] [options]' . PHP_EOL
            . PHP_EOL
            . 'Specify one of these actions:' . PHP_EOL
            . '  --help                   Shows this page' . PHP_EOL
            . '  --execute                Executes all migrations' . PHP_EOL
            . '  --generate               Generates a new migration file' . PHP_EOL
            . '  --up [version]           Installs a specific version' . PHP_EOL
            . '  --down [version]         Uninstalls a specific version' . PHP_EOL
            . PHP_EOL
            . 'These arguments are all optional:' . PHP_EOL
            . '  --config-file [path]     Loads an additional configuration file' . PHP_EOL
            . '  --config [key] [value]   Adds an additional config flag' . PHP_EOL
            . PHP_EOL
        ;

        $this->console->stdout($text);
        return 0;
    }

    /**
     * Dummy method; help has no config requirements
     *
     * @return void
     */
    protected function validateConfig(): void
    {
        // Do nothing; help has no config requirements
    }
}
