<?php

namespace Garrcomm\EasyMigrations\Action;

use Garrcomm\EasyMigrations\Migrations\AbstractMigration;
use RuntimeException;

class Execute extends AbstractAction
{
    public function run(): int
    {
        $dbVersions = $this->engine->getVersions();
        $fsVersions = $this->readFileSystem($this->migrationsPath);
        ksort($fsVersions); // Just in case

        // Checks if the filesystem is complete. Otherwise, something is wrong.
        $error = false;
        foreach ($dbVersions as $version) {
            if (!isset($fsVersions['v' . $version])) {
                $error = true;
                $this->console->stderr('Filesystem misses ' . $version . PHP_EOL);
            }
        }
        // Return when there's an error in the above, but only after showing all misses.
        if ($error) {
            return 1;
        }

        // Collects a list of versions to install
        foreach ($fsVersions as $version => $file) {
            $version = substr($version, 1); // Removes the initial v, added to use numeric keys more safely.
            if (!in_array($version, $dbVersions)) {
                require_once $file;
                $migrationClass = $this->migrationsNamespace . '\\Version' . $version;
                if (!class_exists($migrationClass)) {
                    throw new RuntimeException('Migration ' . $version . ' seems to be broken; no class exists');
                }
                if (!is_subclass_of($migrationClass, AbstractMigration::class)) {
                    throw new RuntimeException(
                        'Migration ' . $version . ' doesn\'t extend ' . AbstractMigration::class
                    );
                }
                $this->console->stdout('Executing up on ' . $version . '...' . PHP_EOL);
                $this->engine->up(new $migrationClass());
            }
        }
        $this->console->stdout('Done!' . PHP_EOL);

        return 0;
    }

    /**
     * Reads all versions from the filesystem recursively; array keys are prefixed with a 'v'.
     *
     * @param string $path Path to read.
     *
     * @return array<string, string>
     */
    protected function readFileSystem(string $path): array
    {
        $return = [];
        $dirs = glob($path . '/*', GLOB_ONLYDIR);
        if ($dirs === false) {
            throw new RuntimeException('Can\'t read dirs from ' . $path);
        }
        foreach ($dirs as $dir) {
            $return = array_merge($return, $this->readFileSystem($dir));
        }
        $files = glob($path . '/*.php');
        if ($files === false) {
            throw new RuntimeException('Can\'t read files from ' . $path);
        }
        foreach ($files as $file) {
            preg_match('/\/Version([0-9]*).php$/', $file, $matches);
            if (isset($matches[1])) {
                $return['v' . $matches[1]] = $file;
            }
        }
        return $return;
    }
}
