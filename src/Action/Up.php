<?php

namespace Garrcomm\EasyMigrations\Action;

use Garrcomm\EasyMigrations\Migrations\AbstractMigration;
use InvalidArgumentException;
use RuntimeException;

class Up extends AbstractAction
{
    /**
     * @var int
     */
    private $version;

    public function setVersion(int $version): self
    {
        $this->version = $version;
        return $this;
    }

    public function run(): int
    {
        $path = $this->migrationsPath . '/' . substr((string)$this->version, 0, 4)
            . '/' . substr((string)$this->version, 4, 2) . '/Version' . $this->version . '.php';
        if (!file_exists($path)) {
            throw new InvalidArgumentException('Version not found: ' . $path);
        }

        require_once $path;
        $migrationClass = $this->migrationsNamespace . '\\Version' . $this->version;
        if (!class_exists($migrationClass)) {
            throw new RuntimeException('Migration ' . $this->version . ' seems to be broken; no class exists');
        }
        if (!is_subclass_of($migrationClass, AbstractMigration::class)) {
            throw new RuntimeException(
                'Migration ' . $this->version . ' doesn\'t extend ' . AbstractMigration::class
            );
        }
        $this->console->stdout('Executing up on ' . $this->version . '...' . PHP_EOL);
        $this->engine->up(new $migrationClass());
        $this->console->stdout('Done!' . PHP_EOL);

        return 0;
    }
}
