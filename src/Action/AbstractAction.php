<?php

namespace Garrcomm\EasyMigrations\Action;

use Garrcomm\EasyMigrations\Engine\AbstractEngine;
use Garrcomm\EasyMigrations\Model\Config;
use Garrcomm\EasyMigrations\Service\ConsoleOutput;
use RuntimeException;
use InvalidArgumentException;

abstract class AbstractAction
{
    /**
     * The database engine
     *
     * @var AbstractEngine
     */
    protected $engine;

    /**
     * Folder for migration storage
     *
     * @var string
     */
    protected $migrationsPath;

    /**
     * Namespace for the migrations
     *
     * @var string
     */
    protected $migrationsNamespace;

    /**
     * Reference to the config
     *
     * @var Config
     */
    protected $config;

    /**
     * Reference to the console output
     *
     * @var ConsoleOutput
     */
    protected $console;

    /**
     * Initiates a new action
     *
     * @param Config        $config  Reference to the config.
     * @param ConsoleOutput $console Reference to the console output.
     */
    public function __construct(Config $config, ConsoleOutput $console)
    {
        $this->config = $config;
        $this->console = $console;
        $this->validateConfig();
    }

    /**
     * Executes the action and returns an exit code
     *
     * @return int
     */
    abstract public function run(): int;

    /**
     * Validates config and populates properties of this class
     *
     * @return void
     */
    protected function validateConfig(): void
    {
        // Validates the path
        if (!isset($this->config['path'])) {
            throw new RuntimeException('No config value "path" defined');
        }
        $this->migrationsPath = rtrim($this->config['path']->getValueAsPath(), '/\\');

        // Sets the namespace
        if (isset($this->config['namespace'])) {
            $this->migrationsNamespace = rtrim($this->config['namespace']->getValue(), '\\');
        } else {
            $this->migrationsNamespace = 'Migrations';
        }

        // Validates and configures the engine
        if (!isset($this->config['engine'])) {
            throw new InvalidArgumentException('No engine configured.');
        }
        $engineClass = 'Garrcomm\\EasyMigrations\\Engine\\' . ucfirst($this->config['engine']);
        if (!class_exists($engineClass)) {
            throw new InvalidArgumentException('Unknown engine: ' . $this->config['engine']);
        }
        if (!is_subclass_of($engineClass, AbstractEngine::class)) {
            throw new RuntimeException('Invalid engine: ' . $this->config['engine']);
        }
        $this->engine = new $engineClass($this->config);
    }
}
