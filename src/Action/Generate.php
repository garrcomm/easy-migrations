<?php

namespace Garrcomm\EasyMigrations\Action;

use RuntimeException;

class Generate extends AbstractAction
{
    /**
     * Chmod values for folders
     *
     * @var int
     */
    private $pathAttributes;

    /**
     * Chmod values for files
     *
     * @var int
     */
    private $fileAttributes;

    public function run(): int
    {
        $fullPath = $this->migrationsPath . '/' . date('Y') . '/' . date('m');
        if (!is_dir($fullPath)) {
            mkdir($fullPath, $this->pathAttributes, true);
        }
        if (!is_dir($fullPath)) {
            throw new RuntimeException('Can\'t create folder ' . $fullPath);
        }

        $version = date('YmdHis');
        $fullFile = $fullPath . '/Version' . $version . '.php';
        if (file_exists($fullFile)) {
            throw new RuntimeException($fullFile . ' already exists. Please wait one second.');
        }
        $template = file_get_contents(__DIR__ . '/../../template/migration.php.tpl');
        if (!$template) {
            throw new RuntimeException('Can\'t open ' . $template);
        }
        $template = str_replace('[[namespace]]', $this->migrationsNamespace, $template);
        $template = str_replace('[[classname]]', 'Version' . $version, $template);

        file_put_contents($fullFile, $template);
        chmod($fullFile, $this->fileAttributes);

        $this->console->stdout('File ' . $fullFile . ' created' . PHP_EOL);

        return 0;
    }

    /**
     * Validates config and populates properties of this class
     *
     * @return void
     */
    protected function validateConfig(): void
    {
        parent::validateConfig();

        // Validates the path attributes
        if (isset($this->config['path_attr'])) {
            $this->pathAttributes = $this->config['path_attr']->getValueAsAttr();
        } else {
            // Default value
            $this->pathAttributes = 0755;
        }

        // Validates the file attributes
        if (isset($this->config['file_attr'])) {
            $this->fileAttributes = $this->config['file_attr']->getValueAsAttr();
        } else {
            // Default value
            $this->fileAttributes = 0755;
        }
    }
}
