<?php

namespace Garrcomm\EasyMigrations\Engine;

use Garrcomm\EasyMigrations\Model\Config;
use InvalidArgumentException;
use mysqli as connection;
use mysqli_sql_exception;
use RuntimeException;

class Mysqli extends AbstractEngine
{
    /**
     * MySQLi connection
     *
     * @var connection
     */
    private $mysqli;

    /**
     * Migrations table
     *
     * @var string
     */
    private $table;

    public function __construct(Config $config)
    {
        parent::__construct($config);
        if (!isset($config['database'])) {
            throw new InvalidArgumentException('Config value "database" is required');
        }
        if (!class_exists(connection::class)) {
            throw new RuntimeException('The PHP extension mysqli is not available');
        }

        // Use strict reporting
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

        // Custom table name
        $this->table = isset($config['version_table']) ? $config['version_table']->getValue() : '_migration_versions';

        $this->mysqli = new connection(
            isset($config['hostname']) ? $config['hostname']->getValue() : null,
            isset($config['username']) ? $config['username']->getValue() : null,
            isset($config['password']) ? $config['password']->getValue() : null,
            null,
            isset($config['port']) ? (int)$config['port']->getValue() : null,
            isset($config['socket']) ? $config['socket']->getValue() : null,
        );

        // Creates the database with versioning table, if it doesn't yet exist
        $this->mysqli->query("CREATE DATABASE IF NOT EXISTS `" . $config['database'] . "`;");
        $this->mysqli->query("USE `" . $config['database'] . "`;");
        $this->mysqli->query("CREATE TABLE IF NOT EXISTS `" . $this->table . "` (
            `version` varchar(14) NOT NULL,
            `executed_at` datetime NOT NULL,
            PRIMARY KEY (`version`)
        ) ENGINE=InnoDB;");
    }

    /**
     * Returns a list of all available versions
     *
     * @return int[]
     */
    public function getVersions(): array
    {
        $return = array();
        $result = $this->mysqli->query("SELECT `version` FROM `" . $this->table . "` ORDER BY `version` ASC;");
        if (is_bool($result)) {
            throw new RuntimeException('Can\'t fetch versions');
        }
        while ($row = $result->fetch_row()) {
            $return[] = (int)$row[0];
        }

        return $return;
    }

    protected function installVersion(int $version, array $queries): void
    {
        $this->mysqli->begin_transaction();
        try {
            foreach ($queries as $query) {
                $this->mysqli->query($query);
            }
            $this->mysqli->query(
                "INSERT INTO `" . $this->table . "` SET `version` = '"
                . $this->mysqli->real_escape_string((string)$version) . "', `executed_at` = NOW();"
            );
            $this->mysqli->commit();
        } catch (mysqli_sql_exception $exception) {
            $this->mysqli->rollback();
            throw $exception;
        }
    }

    protected function uninstallVersion(int $version, array $queries): void
    {
        $this->mysqli->begin_transaction();
        try {
            foreach ($queries as $query) {
                $this->mysqli->query($query);
            }
            $this->mysqli->query(
                "DELETE FROM `" . $this->table . "` WHERE `version` = '"
                . $this->mysqli->real_escape_string((string)$version) . "';"
            );
            $this->mysqli->commit();
        } catch (mysqli_sql_exception $exception) {
            $this->mysqli->rollback();
            throw $exception;
        }
    }
}
