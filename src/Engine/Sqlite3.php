<?php

namespace Garrcomm\EasyMigrations\Engine;

use Garrcomm\EasyMigrations\Model\Config;
use SQLite3 as connection;

class Sqlite3 extends AbstractEngine
{
    /**
     * SQLite3 connection
     *
     * @var connection
     */
    private $sqlite3;

    /**
     * Migrations table
     *
     * @var string
     */
    private $table;

    public function __construct(Config $config)
    {
        parent::__construct($config);
        if (!isset($config['database'])) {
            throw new InvalidArgumentException('Config value "database" is required');
        }
        if (!class_exists(connection::class)) {
            throw new RuntimeException('The PHP extension SQLite3 is not available');
        }

        // Custom table name
        $this->table = isset($config['version_table']) ? $config['version_table']->getValue() : '_migration_versions';

        // Creates the database with versioning table, if it doesn't yet exist
        $this->sqlite3 = new connection(
            $config['database']->getValueAsPath(),
            SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE,
            isset($config['encryption_key']) ? $config['encryption_key']->getValue() : ''
        );
        $this->sqlite3->query("CREATE TABLE IF NOT EXISTS `" . $this->table . "` (
            `version` varchar(14) NOT NULL,
            `executed_at` datetime NOT NULL,
            PRIMARY KEY (`version`)
        );");
        if (isset($config['database_attr'])) {
            chmod($config['database']->getValueAsPath(), $config['database_attr']->getValueAsAttr());
        }
    }

    /**
     * Returns a list of all available versions
     *
     * @return int[]
     */
    public function getVersions(): array
    {
        $return = array();
        $result = $this->sqlite3->query("SELECT `version` FROM `" . $this->table . "` ORDER BY `version` ASC;");
        if (is_bool($result)) {
            throw new RuntimeException('Can\'t fetch versions');
        }
        while ($row = $result->fetchArray(SQLITE3_NUM)) {
            $return[] = (int)$row[0];
        }

        return $return;
    }

    protected function installVersion(int $version, array $queries): void
    {
        foreach ($queries as $query) {
            $this->sqlite3->query($query);
        }
        $this->sqlite3->query(
            "INSERT INTO `" . $this->table . "` (`version`, `executed_at`) VALUES " .
            "('" . connection::escapeString((string)$version) . "', '" . date('Y-m-d H:i:s') . "');");
    }

    protected function uninstallVersion(int $version, array $queries): void
    {
        foreach ($queries as $query) {
            $this->sqlite3->query($query);
        }
        $this->sqlite3->query(
            "DELETE FROM `" . $this->table . "` WHERE `version` = '"
            . connection::escapeString((string)$version) . "';"
        );
    }
}