<?php

namespace Garrcomm\EasyMigrations\Engine;

use Garrcomm\EasyMigrations\Migrations\AbstractMigration;
use Garrcomm\EasyMigrations\Model\Config;

abstract class AbstractEngine
{
    /**
     * @var Config
     */
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Returns a list of all available versions
     *
     * @return int[]
     */
    abstract public function getVersions(): array;

    /**
     * Installs a migration
     *
     * @param AbstractMigration $migration The migration.
     *
     * @return void
     */
    public function up(AbstractMigration $migration): void
    {
        // Collects queries
        $migration->up();
        $this->installVersion($migration->getVersion(), $migration->getQueries());
    }

    /**
     * Uninstalls a migration
     *
     * @param AbstractMigration $migration The migration.
     *
     * @return void
     */
    public function down(AbstractMigration $migration): void
    {
        // Collects queries
        $migration->down();
        $this->uninstallVersion($migration->getVersion(), $migration->getQueries());
    }

    /**
     * Installs a version
     *
     * @param int      $version The version.
     * @param string[] $queries List of queries for this version.
     *
     * @return void
     */
    abstract protected function installVersion(int $version, array $queries): void;

    /**
     * Uninstalls a version
     *
     * @param int      $version The version.
     * @param string[] $queries List of queries for this version.
     *
     * @return void
     */
    abstract protected function uninstallVersion(int $version, array $queries): void;
}
