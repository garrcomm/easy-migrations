<?php

namespace Garrcomm\EasyMigrations\Model;

class ConfigValue
{
    /**
     * Config key
     *
     * @var string
     */
    private $key;
    /**
     * Config value
     *
     * @var string
     */
    private $value;
    /**
     * Source of the config (can be an executable or an ini file)
     * @var string
     */
    private $sourcePath;
    /**
     * @var int The priority, the higher, the more important, or -1 for overruling
     */
    private $priority;

    /**
     * Initiates a config flag
     *
     * @param string $key        Config key.
     * @param string $value      Config value.
     * @param string $sourcePath Source of the config (can be an executable or an ini file).
     * @param int    $priority   The priority, the higher, the more important, or -1 for overruling.
     */
    public function __construct(string $key, string $value, string $sourcePath, int $priority)
    {
        $this->key = $key;
        $this->value = $value;
        $this->sourcePath = (string)realpath($sourcePath);
        $this->priority = $priority;
    }

    /**
     * Returns the config key as string
     *
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * Returns the config value as string
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * Returns the config value as attr (for chmod permissions)
     *
     * @return int
     */
    public function getValueAsAttr(): int
    {
        if (strlen($this->value) > 3) {
            return (int)$this->value;
        } else {
            return (int)octdec('0' . $this->value);
        }
    }

    /**
     * Returns the priority of the config entry
     *
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * Returns the config value as absolute path
     *
     * @return string
     */
    public function getValueAsPath(): string
    {
        // Absolute path should be returned as-is
        if (substr($this->getValue(), 0, 1) == '/' || substr($this->getValue(), 1, 1) == ':') {
            return $this->getValue();
        }

        // Prefix source path to config value
        $return = pathinfo($this->sourcePath, PATHINFO_DIRNAME) . '/' . $this->getValue();
        if (file_exists($return)) {
            $return = realpath($return);
        }
        return $return;
    }

    /**
     * Returns the config value as string
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getValue();
    }
}
