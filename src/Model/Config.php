<?php

namespace Garrcomm\EasyMigrations\Model;

use Countable;
use ArrayAccess;
use RuntimeException;

class Config implements ArrayAccess, Countable
{
    /**
     * Array that holds all config items internally
     *
     * @var array<string, ConfigValue>
     */
    private $config = array();

    /**
     * Adds a config flag
     *
     * @param string $key        Config key.
     * @param string $value      Config value.
     * @param string $sourcePath Source of the config (can be an executable or an ini file).
     * @param int    $priority   The priority, the higher, the more important, or -1 for overruling.
     *
     * @return void
     */
    public function addConfigFlag(string $key, string $value, string $sourcePath, int $priority): void
    {
        // When the value already exists and has the absolute priority (-1) or a priority higher than the actual one,
        // don't overwrite the config.
        if (isset($this->config[$key])) {
            if ($this->config[$key]->getPriority() < 0 || $this->config[$key]->getPriority() > $priority) {
                return;
            }
        }
        $this->config[$key] = new ConfigValue($key, $value, $sourcePath, $priority);
    }

    /**
     * Whether an offset exists
     *
     * @param mixed $offset An offset to check for.
     *
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return isset($this->config[$offset]);
    }

    /**
     * Offset to retrieve
     *
     * @param mixed $offset The offset to retrieve.
     *
     * @return ConfigValue
     */
    public function offsetGet($offset): ConfigValue
    {
        return $this->config[$offset];
    }

    /**
     * Assign a value to the specified offset
     *
     * @param mixed $offset The offset to assign the value to.
     * @param mixed $value  The value to set.
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        throw new RuntimeException('To set config values, use addConfigFlag');
    }

    /**
     * Unset an offset
     *
     * @param mixed $offset The offset to unset.
     *
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        throw new RuntimeException('Config flags can\'t be unset');
    }

    /**
     * Count elements of an object
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->config);
    }
}
