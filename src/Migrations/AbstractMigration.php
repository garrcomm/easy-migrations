<?php

namespace Garrcomm\EasyMigrations\Migrations;

abstract class AbstractMigration
{
    /**
     * List of queries to execute within a transaction
     *
     * @var string[]
     */
    private $queries = array();

    /**
     * Inserts up() queries
     *
     * @return void
     */
    abstract public function up(): void;

    /**
     * Inserts down() queries
     *
     * @return void
     */
    abstract public function down(): void;

    /**
     * Adds a query to the transaction
     *
     * @param string $query The query.
     *
     * @return void
     */
    protected function addQuery(string $query): void
    {
        $this->queries[] = $query;
    }

    /**
     * Retrieves a list of all queries
     *
     * @return string[]
     */
    final public function getQueries(): array
    {
        return $this->queries;
    }

    /**
     * Returns the version based on the classname
     *
     * @return int
     */
    final public function getVersion(): int
    {
        preg_match('/Version([0-9]{14})/', get_called_class(), $matches);
        return (int)$matches[1];
    }
}
