#!/usr/bin/env bash
cd `dirname $0`
cd ..
docker run --rm --volume "${PWD}:/var/www/html" garrcomm/php-apache-composer composer $@
