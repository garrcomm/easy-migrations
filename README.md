# Easy Migrations

For simpler applications with a database, complicated ORM like migrations are overkill, but database versioning is still very useful.

For that purpose, I created this package.

## Configuration file

The configuration file is in [ini format](https://en.wikipedia.org/wiki/INI_file).  
There are a few default locations where configuration files should be located.
It's also possible to specify a config file with the `--config-file [path]` argument,
or by specifying a specific config value with the `--config [key] [value]` argument.

If more then one config file is found and/or specified, they will be loaded in this sequence:

1. migrator.ini.dist in the current working directory
2. migrator.ini in the current working directory
3. migrator.ini.dist in the current working directory plus "/config/"
4. migrator.ini in the current working directory plus "/config/"
5. migrator.ini.dist in the directory containing the migrator executable
6. migrator.ini in the directory containing the migrator executable
7. All config files specified by `--config-file [path]`
8. All flags specified by `--config [key] [value]`

A config file for MySQLi can look like this:

```ini
[migrator]

; The path (relative from the configuration file) and namespace in which migrations will be added
path = app/migrations
;namespace = Migrations

; Default chmod for folders
;path_attr = 0755
; Default chmod for files
;file_attr = 0644

engine = mysqli

; MySQLi connection info
hostname = localhost
username = root
password = rootroot
database = mydatabase
port = 3306
;socket = /var/run/mysqld.sock

; Name of the versioning table
;version_table = _migration_versions
```

For SQLite3, the configation could be like this:

```ini
[migrator]

; The path (relative from the configuration file) and namespace in which migrations will be added
path = app/migrations
;namespace = Migrations

; Default chmod for folders
;path_attr = 0755
; Default chmod for files
;file_attr = 0644

engine = sqlite3

database = /private/database.db
; database_attr = 0644
; encryption_key = foobar

; Name of the versioning table
;version_table = _migration_versions
```

Sections other than `[migrator]` are ignored and values added with `--config [key] [value]` are interpreted as if they existed in the `[migrator]` section.

Values that are commented out in the example above are default values.
